# Chamada-API 

## Chamada-API é um backend responsável por controlar a chamada de alunos.

<img src="./docs/hands.jpeg" align="right" width="245" style="z-index:999">

# Sumário

- [Pré-requisitos](#Pré-requisitos)
- [Configuração](#Configuração)
- [Testes](#Testes)
- [Executar](#Executar)

# Pré-requisitos

- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)

# Configuração

**Faça um clone do projeto:**

https<br/>
https://gitlab.com/WelberSilverio/chamada-api.git

ssh<br/>
git@gitlab.com:WelberSilverio/chamada-api.git

**Instale as dependências do projeto:**<br/>
Caso não tenha yarn instalado, seguir o guia de instalação contido na seção _pré requisitos_ ou executar o comando: `npm i -g yarn`.<br/>
A versão do node utilizada neste projeto foi 12.6.0.<br>
Depois que o projeto foi clonado execute o comando abaixo na raiz do projeto. <br/>

```sh
yarn
```

# Testes

Para executar os testes do projeto, execute o comando abaixo

```sh
yarn test
```

Foram criados testes unitários e integrados, onde 100% do código deve estar coberto.

# Executar

Criar o arquivo `.env` na raiz do projeto. Pode copiar o conteúdo do arquivo `.env.example` para o arquivo criado `.env`.
O projeto pode ser executado pelo Visual Studio apertando _F5_ ou pelo comando abaixo

```sh
yarn watch
```
