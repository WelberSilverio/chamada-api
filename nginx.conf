events {
    worker_connections  1024;
}
http {
    # access_log /dev/stdout; # Log in file. Or use: "teresa app logs [APP] --container nginx -f"
    server {
        real_ip_header X-Forwarded-For;
        real_ip_recursive on;
        set_real_ip_from 0.0.0.0/0;
        listen $NGINX_PORT;
        location / {
            allow 200.174.209.0/24;
            # Google Heath Check
            allow 35.191.0.0/16;
            allow 130.211.0.0/22;
            allow 10.221.0.0/16;
            allow 10.226.0.0/17;

            # Nodes address range - gke-multicanalidade-transaction
            allow 10.215.129.0/25;

            # Nodes address range - gke-multicanalidade-worker
            allow 10.215.130.0/25;

            # Nodes address range - gke-pontos-fisicos
            allow 10.221.32.0/21;

            # Pod address range - gke-pontos-fisicos
            allow 10.221.48.0/20;

            # Service address range - gke-pontos-fisicos
            allow 10.221.40.0/21;

            # Nodes address range - gke-stores-transaction
            allow 10.215.128.128/25;

            # Pod address range - gke-stores-transaction
            allow 10.215.96.0/19;

            # Service address range - gke-stores-transaction
            allow 10.215.64.0/19;

            # Pod address range - gke-multicanalidade-transaction
            allow 10.216.64.0/19;

            # Service address range - gke-multicanalidade-transaction
            allow 10.216.96.0/19;

            # Pod address range - gke-multicanalidade-worker
            allow 10.216.128.0/19;

            # Service address range - gke-multicanalidade-worker
            allow 10.215.130.128/25;



            # southamerica-east1 subnet
            allow 10.158.0.0/20;

            ## subnets do cluster
            allow 10.8.0.0/14;

            # NGINX_INGRESS stage
            allow 10.142.0.0/16;

            # Apigee
            allow 35.227.17.210/32;
            allow 35.237.94.175/32;
            allow 104.196.170.4/32;
            allow 35.231.90.125/32;
            allow 35.233.189.20/32;
            allow 35.227.148.126/32;
            allow 35.185.208.243/32;
            allow 35.233.150.95/32;
            allow 35.199.100.99/32;
            allow 35.198.0.180/32;
            allow 35.199.108.50/32;
            allow 35.198.59.115/32;
            allow 35.199.71.211/32;
            allow 35.199.81.158/32;
            allow 35.198.1.183/32;
            allow 35.199.84.4/32;

            # HB
            allow 177.69.78.32/27;
            allow 200.174.209.224/28;

            # Lojas
            allow 200.170.150.208/28;
            allow 189.57.175.24/29;

            # ENSP
            allow 187.72.156.160/28;
            allow 200.206.44.48/29;

            # Labs Sampa
            allow 187.9.83.211/29;
            allow 187.12.176.3/29;

            #Clusters gcp
            allow 34.74.38.51/32;
            allow 35.231.209.95/32;
            allow 35.229.75.14/32;
            allow 35.198.7.41/32;
            allow 35.247.194.60/32;
            allow 35.247.195.14/32;
            allow 35.198.25.57/32;
            allow 35.199.87.183/32;
            allow 35.247.250.195/32;
            # Clusters pontos-fisicos-a
            allow 35.198.7.41/32;
            allow 35.247.194.60/32;
            allow 35.247.195.14/32;
            allow 35.198.25.57/32;
            allow 35.199.87.183/32;
            allow 35.247.250.195/32;
            allow 35.247.211.43/32;
            allow 34.95.254.1/32;
            allow 35.199.109.109/32;
            allow 34.95.148.26/32;
            allow 35.199.64.80/32;
            allow 34.95.208.191/32;
            allow 34.95.164.182/32;
            allow 34.95.148.158/32;
            allow 35.247.225.222/32;

            # Apenas para aplicações do cluster production-pontos-fisicos-a
            allow 10.221.208.0/21;
            allow 10.221.216.0/21;
            allow 10.221.224.0/21;
            allow 10.221.232.0/21;
            allow 10.226.0.0/17;
            allow 10.226.128.0/17;

            # VPNs
            allow 200.206.44.52/32;
            allow 201.48.213.10/32;
            allow 200.174.209.229/32;
            allow 177.69.78.39/32;

            #Carrefour
            allow 177.222.16.150/32;
            allow 177.222.16.146/32;

            ##Demais subnets
            deny all;
            client_max_body_size 5M;
            proxy_pass $NGINX_BACKEND;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Host $http_host;
            proxy_set_header X-Request-Start "t=${msec}";
        }
    }
}
