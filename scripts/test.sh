#!/bin/sh
set -e
export NODE_ENV="test"

cd "$(dirname "$0")/.."

echo "===> Build app"
npm run build

# reset database to a fresh state.
node_modules/.bin/sequelize db:migrate:undo:all
node_modules/.bin/sequelize db:migrate

# Seed database
node_modules/.bin/sequelize db:seed:undo:all
node_modules/.bin/sequelize db:seed:all

echo "===> Run Tests"
node_modules/.bin/jest --detectOpenHandles --coverage --forceExit
