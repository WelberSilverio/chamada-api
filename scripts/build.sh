set -e

cd "$(dirname "$0")/.."

echo "===> Build the project "
rm -rf build && node_modules/.bin/tsc -p tsconfig.json
