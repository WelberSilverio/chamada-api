export enum EEvents {
  CREATE_ORDER = 1,
  PAYMENT = 2,
  PARTNER_INTEGRATION = 3,
  WELCOME_MESSAGE = 4,
  SCHEDULED = 5,
  REMINDER_DAY_BEFORE = 6,
  ON_THE_WAY = 7,
  FINISHED_SUCCESS = 8,
  MISS_VISIT = 9,
  CANCEL_ORDER = 10,
  FINISHED_PARTS = 11,
  RESCHEDULED = 12,
  NO_SHOW = 13,
  ORDER_REJECTED = 15,
  ORDER_PAID = 16,
}
