/* istanbul ignore file */

import { Controller, IHttp, Get } from 'above-framework';
import HTTPStatus from 'http-status';

@Controller('/health')
class HealthController {
  @Get('/ping', {
    description: 'Verificar saúde da aplicação',
    auth: false,
  })
  public async getBalance({ reply }: IHttp) {
    try {
      return reply.response({
        status: 'pong',
      });
    } catch (e) {
      return reply
        .response({
          status: 'error',
        })
        .code(HTTPStatus.INTERNAL_SERVER_ERROR);
    }
  }
}

export default HealthController;
