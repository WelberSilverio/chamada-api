import { decode } from 'base64-arraybuffer';
import Roll from '../../../models/roll';
import SchoolClassStudent from '../../../models/schoolclass-student';
import Student from '../../../models/student';
import RollNotFoundException from './exceptions/roll-not-found-exception';
import SchoolClass from '../../../models/schoolclass';
import { IRoll, IRollByImage, IRollCreated } from './interfaces/roll';
import FaceApiRequest from '../../../requests/face-api/face-api-request';
import StudentNotFoundException from '../student/exceptions/student-not-found-exception';

class RollBusiness {
  public getByQuery = async (classDate?: string, studentId?: number, schoolclassId?: number) => {
    const rolls = ((await Roll.findAll({
      attributes: ['id', 'classDate', 'status'],
      where: { ...(classDate ? { classDate } : {}) },
      include: [
        {
          attributes: ['id'],
          model: SchoolClassStudent,
          where: {
            ...(studentId ? { studentId } : {}),
            ...(schoolclassId ? { schoolclassId } : {}),
          },
          include: [
            {
              model: Student,
              attributes: ['id', 'name'],
            },
            {
              model: SchoolClass,
              attributes: ['id', 'name'],
            },
          ],
        },
      ],
    })) as unknown) as IRoll[];
    if (rolls.length === 0) {
      throw new RollNotFoundException();
    }
    return rolls;
  };

  public createRollByImg = async (params: IRollByImage) => {
    const image = decode(params.image);
    const faces = await FaceApiRequest.detectFace(image);
    const foundFace = await FaceApiRequest.findSimilars(faces);
    const schoolClassStudent = await (SchoolClassStudent.findOne({
      where: { schoolclassId: params.schoolClass.id },
      include: [
        {
          model: Student,
          attributes: [],
          where: { faceId: foundFace },
        }
      ]
    })as unknown) as SchoolClassStudent;
    if (!schoolClassStudent){
      throw new StudentNotFoundException();
    }

    return await Roll.create({
      schoolclassStudentId: schoolClassStudent.id,
      classDate: params.date,
      status: 'PRESENTE',
    });
  };

  public create = async (roll: IRollCreated) => {
    return await Roll.create({
      schoolclassStudentId: roll.schoolClassStudent.id,
      classDate: roll.date,
      status: roll.status,
    });
  };
}


export default RollBusiness;
