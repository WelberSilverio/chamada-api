import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class RollNotFoundException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Roll not found',
      errorCode: 20023,
      statusCode: HTTPStatus.NOT_FOUND,
      userMessage: 'You attempted to get a Roll, but did not find any',
    });
  }
}

export default RollNotFoundException;
