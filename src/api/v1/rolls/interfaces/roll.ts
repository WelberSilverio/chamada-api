export interface IRoll {
  id: number;
  classDate: string;
  status: string;
  schoolClassStudent: {
    id: number;
    student: {
      id: number;
      name: string;
    };
    schoolclass: {
      id: number;
      name: string;
    };
  };
}

export interface IRollByImage {
  image: string;
  schoolClass: {
    id: number;
  };
  date: string;
}

export interface IRollCreated {
  status: string;
  schoolClassStudent: {
    id: number;
  };
  date: string;
}
