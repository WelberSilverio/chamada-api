import { Controller, IHttp, Get, Post } from 'above-framework';
import HTTPStatus from 'http-status';

import RollBusiness from './roll.business';
import { RollSchema } from './roll.schema';

@Controller('/rolls')
class RollController {
  // @ts-ignore
  @Get('/', {
    description: 'Rota para encontrar presenças por parametros',
    validate: RollSchema.getByQuery,
    records: true,
  })
  public async getByQuery({
    reply,
    request: {
      query: { date, 'student.id': studentId, 'schoolclass.id': schoolclassId },
    },
  }: IHttp<typeof RollSchema.getByQuery>) {
    const rolls = await new RollBusiness().getByQuery(date, studentId, schoolclassId);
    return reply.response(rolls).code(HTTPStatus.OK);
  }

  // @ts-ignore
  @Post('/', {
    description: 'Rota para dar presença a aluno por foto',
    validate: RollSchema.createRollByImg,
    records: true,
  })
  public async createRollByImg({ reply, request: { payload } }: IHttp<typeof RollSchema.createRollByImg>) {
    const roll = await new RollBusiness().createRollByImg(payload);
    return reply.response(roll).code(HTTPStatus.CREATED);
  }

  // @ts-ignore
  @Post('/simpleCreate/', {
    description: 'Rota para dar presença a aluno por foto',
    validate: RollSchema.create,
    records: true,
  })
  public async create({ reply, request: { payload } }: IHttp<typeof RollSchema.create>) {
    const roll = await new RollBusiness().create(payload);
    return reply.response(roll).code(HTTPStatus.CREATED);
  }
}

export default RollController;
