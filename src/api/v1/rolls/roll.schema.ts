import { Joi } from 'above-framework';

export const RollSchema = {
  getByQuery: {
    query: Joi.object()
      .keys({
        date: Joi.string(),
        'student.id': Joi.number(),
        'schoolclass.id': Joi.number(),
      })
      .required(),
  },
  createRollByImg: {
    payload: Joi.object()
      .keys({
        schoolClass: Joi.object({
          id: Joi.number().required(),
        }).required(),
        image: Joi.string().required(),
        date: Joi.string().required(),
      })
      .required(),
  },
  create: {
    payload: Joi.object()
      .keys({
        schoolClassStudent: Joi.object({
          id: Joi.number().required(),
        }).required(),
        date: Joi.string().required(),
        status: Joi.string().required(),
      })
      .required(),
  },
};
