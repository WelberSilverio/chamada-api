import SchoolClass from '../../../models/schoolclass';
import { ISchoolClass } from './interfaces/schoolclass';
import SchoolClassNotFoundException from './exceptions/schoolclass-not-found-exception';

class SchoolClassBusiness {
  public getByQuery = async (teacherId: number, day: string, shift?: string, period?: string) => {
    const listSchoolClasses = ((await SchoolClass.findAll({
      attributes: ['id', 'name', 'day', 'shift', 'period'],
      where: {
        teacherId,
        day,
        ...(shift ? { shift } : {}),
        ...(period ? { period } : {}),
      },
    })) as unknown) as ISchoolClass[];
    if (listSchoolClasses.length === 0) {
      throw new SchoolClassNotFoundException();
    }
    return listSchoolClasses;
  };
}

export default SchoolClassBusiness;
