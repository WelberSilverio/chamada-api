import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class SchoolClassNotFoundException extends BaseException {
  constructor() {
    super({
      developerMessage: 'School Class not found',
      errorCode: 20023,
      statusCode: HTTPStatus.NOT_FOUND,
      userMessage: 'You attempted to get a School Class, but did not find any',
    });
  }
}

export default SchoolClassNotFoundException;
