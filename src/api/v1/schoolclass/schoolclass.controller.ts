import { Controller, Get, IHttp } from 'above-framework';
import HTTPStatus from 'http-status';

import { SchoolClassValidateSchema } from './schoolclass.schema';
import SchoolClassBusiness from './schoolclass.business';

@Controller('/schoolclasses')
class SchoolClassController {
  // @ts-ignore
  @Get('/', {
    description: 'Rota para encontrar aulas por filtro',
    validate: SchoolClassValidateSchema.getByQuery,
    records: true,
  })
  public async getSchoolClassByQuery({
    reply,
    request: {
      query: { 'teacher.id': teacherId, day, shift, period },
    },
  }: IHttp<typeof SchoolClassValidateSchema.getByQuery>) {
    const schoolclasses = await new SchoolClassBusiness().getByQuery(teacherId, day, shift, period);
    return reply.response(schoolclasses).code(HTTPStatus.OK);
  }
}

export default SchoolClassController;
