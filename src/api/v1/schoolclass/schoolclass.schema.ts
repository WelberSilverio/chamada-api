import { Joi } from 'above-framework';

export const SchoolClassValidateSchema = {
  getByQuery: {
    query: Joi.object()
      .keys({
        'teacher.id': Joi.number().required(),
        day: Joi.string().required(),
        shift: Joi.string(),
        period: Joi.string(),
      })
      .required(),
  },
};
