export interface ISchoolClass {
  id: number;
  name: string;
  day: string;
  shift: string;
  period: string;
}
