import { Controller, Post, IHttp, Get } from 'above-framework';
import HTTPStatus from 'http-status';

import { SchoolClassStudentValidateSchema } from './schoolclass-student.shema';
import SchoolClassStudentBusiness from './schoolclass-student.business';

@Controller('/schoolclass/student')
class SchoolClassStudentController {
  // @ts-ignore
  @Post('/', {
    description: 'Rota para associar aula a aluno',
    validate: SchoolClassStudentValidateSchema.create,
    records: true,
  })
  public async create({ reply, request: { payload } }: IHttp<typeof SchoolClassStudentValidateSchema.create>) {
    const schoolclassStudent = await new SchoolClassStudentBusiness().create(payload);
    return reply.response(schoolclassStudent).code(HTTPStatus.CREATED);
  }

  // @ts-ignore
  @Get('/', {
    description: 'Rota para buscar alunos de uma aula',
    validate: SchoolClassStudentValidateSchema.findBySchoolClass,
    records: true,
  })
  public async findBySchoolClass({
    reply,
    request: {
      query: { 'schoolclass.id': schoolclassId },
    },
  }: IHttp<typeof SchoolClassStudentValidateSchema.findBySchoolClass>) {
    const schoolclassStudents = await new SchoolClassStudentBusiness().findBySchoolClass(schoolclassId);
    return reply.response(schoolclassStudents).code(HTTPStatus.OK);
  }
}

export default SchoolClassStudentController;
