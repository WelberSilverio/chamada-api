import SchoolClassStudent from '../../../models/schoolclass-student';
import { ISchoolClassStudent, ISchoolClassStudentReturn } from './interfaces/schoolclass-student';
import Student from '../../../models/student';
import SchoolClassNotFoundException from '../schoolclass/exceptions/schoolclass-not-found-exception';

class SchoolClassStudentBusiness {
  public create = async (schoolclassStudent: ISchoolClassStudent) => {
    return await SchoolClassStudent.create({
      schoolclassId: schoolclassStudent.schoolclass.id,
      studentId: schoolclassStudent.student.id,
      status: schoolclassStudent.status,
    });
  };

  public findBySchoolClass = async (schoolclassId: number) => {
    const schoolclassStudents = ((await SchoolClassStudent.findAll({
      attributes: ['id', 'schoolclassId'],
      where: {
        schoolclassId,
        status: 'CURSANDO',
      },
      include: [
        {
          model: Student,
          attributes: ['id', 'name'],
        },
      ],
    })) as unknown) as ISchoolClassStudentReturn[];
    if (schoolclassStudents.length === 0) {
      throw new SchoolClassNotFoundException();
    }
    return schoolclassStudents;
  };
}

export default SchoolClassStudentBusiness;
