export interface ISchoolClassStudent {
  schoolclass: {
    id: number;
  };
  student: {
    id: number;
  };
  status?: string;
}

export interface ISchoolClassStudentReturn {
  id: number;
  schoolclassId: number;
  student: {
    id: number;
    name: string;
  };
}
