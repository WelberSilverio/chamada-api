import { Joi } from 'above-framework';

export const SchoolClassStudentValidateSchema = {
  create: {
    payload: Joi.object()
      .keys({
        schoolclass: Joi.object({
          id: Joi.number().required(),
        }).required(),
        student: Joi.object({
          id: Joi.number().required(),
        }).required(),
        status: Joi.string(),
      })
      .required(),
  },
  findBySchoolClass: {
    query: Joi.object()
      .keys({
        'schoolclass.id': Joi.number().required(),
      })
      .required(),
  },
};
