import { Joi } from 'above-framework';

export const UserValidateSchema = {
  createUser: {
    payload: Joi.object()
      .keys({
        name: Joi.string().required(),
        nickname: Joi.string().required(),
        document: Joi.string().required(),
        password: Joi.string().required(),
        isTeacher: Joi.boolean(),
        active: Joi.boolean(),
      })
      .required(),
  },
  updateUser: {
    payload: Joi.object()
      .keys({
        id: Joi.number().required(),
        name: Joi.string(),
        nickname: Joi.string(),
        document: Joi.string(),
        isTeacher: Joi.boolean(),
        active: Joi.boolean(),
      })
      .required(),
  },
  login: {
    params: Joi.object()
      .keys({
        nickname: Joi.string().required(),
      })
      .required(),
    headers: Joi.object()
      .keys({
        password: Joi.string().required(),
      })
      .unknown(),
  },
};
