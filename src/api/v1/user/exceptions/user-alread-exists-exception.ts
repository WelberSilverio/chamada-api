import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class UserAlreadyExistsException extends BaseException {
  constructor() {
    super({
      developerMessage: 'User already exists',
      errorCode: 20033,
      statusCode: HTTPStatus.CONFLICT,
      userMessage: 'You attempted to create a User, but already exists',
    });
  }
}

export default UserAlreadyExistsException;
