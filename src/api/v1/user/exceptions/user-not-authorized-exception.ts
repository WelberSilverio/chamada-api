import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class UserNotAuthorizedException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Denied! User not authorized',
      errorCode: 30001,
      statusCode: HTTPStatus.UNAUTHORIZED,
      userMessage: 'You attempted to login with a user, but did not authorized',
    });
  }
}

export default UserNotAuthorizedException;
