import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class UserNotFoundException extends BaseException {
  constructor() {
    super({
      developerMessage: 'User not found',
      errorCode: 20023,
      statusCode: HTTPStatus.NOT_FOUND,
      userMessage: 'You attempted to get a user, but did not find any',
    });
  }
}

export default UserNotFoundException;
