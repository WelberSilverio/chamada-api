import { Controller, Post, IHttp, Get, Put } from 'above-framework';
import HTTPStatus from 'http-status';

import { UserValidateSchema } from './user.schema';
import UserBusiness from './user.business';

@Controller('/users')
class UserController {
  // @ts-ignore
  @Post('/', {
    description: 'Rota para incluir usuarios',
    validate: UserValidateSchema.createUser,
    records: true,
  })
  public async createUser({ reply, request: { payload } }: IHttp<typeof UserValidateSchema.createUser>) {
    const user = await new UserBusiness().createUser(payload);
    return reply.response(user).code(HTTPStatus.CREATED);
  }

  // @ts-ignore
  @Put('/', {
    description: 'Rota para alterar usuarios',
    validate: UserValidateSchema.updateUser,
    records: true,
  })
  public async updateUser({ reply, request: { payload } }: IHttp<typeof UserValidateSchema.updateUser>) {
    const user = await new UserBusiness().updateUser(payload);
    return reply.response(user).code(HTTPStatus.OK);
  }

  // @ts-ignore
  @Get('/login/{nickname}', {
    description: 'Rota para logar usuarios',
    validate: UserValidateSchema.login,
    records: true,
  })
  public async login({
    reply,
    request: {
      params: { nickname },
      headers: { password },
    },
  }: IHttp<typeof UserValidateSchema.login>) {
    const user = await new UserBusiness().login(nickname, password);
    return reply.response(user).code(HTTPStatus.OK);
  }
}

export default UserController;
