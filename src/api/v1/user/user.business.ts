import bcrypt from 'bcryptjs';

import User from '../../../models/user';
import { IUser, IUserReturn, IUserUpdate } from './interfaces/user';
import UserNotAuthorizedException from './exceptions/user-not-authorized-exception';
import UserAlreadyExistsException from './exceptions/user-alread-exists-exception';
import UserNotFoundException from './exceptions/user-not-found-exception';

class UserBusiness {
  public createUser = async (user: IUser) => {
    await this.verifyUser(user.document, user.nickname);
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(user.password, salt);
    const createdUser = await User.create({
      name: user.name,
      nickname: user.nickname,
      document: user.document,
      password: hash,
      isTeacher: user.isTeacher,
      active: user.active,
    });
    return (this.mountUserReturn(createdUser) as unknown) as IUserReturn;
  };

  public updateUser = async (user: IUserUpdate) => {
    await this.verifyUpdateStudent(user);
    const { id, name, nickname, document, isTeacher, active } = user;
    await User.update(
      {
        name,
        nickname,
        document,
        isTeacher,
        active,
      },
      {
        where: { id },
      },
    );
    return user;
  };

  public login = async (nickname: string, password: string) => {
    const user = await User.findOne({
      where: {
        nickname,
        active: true,
      },
    });

    if (!user || !bcrypt.compareSync(password, user.password)) {
      throw new UserNotAuthorizedException();
    }

    return (this.mountUserReturn(user) as unknown) as IUserReturn;
  };

  private verifyUser = async (document: string, nickname: string) => {
    const existDocument = await User.findOne({ where: { document } });
    const existNickname = await User.findOne({ where: { nickname } });
    if (existDocument || existNickname) {
      throw new UserAlreadyExistsException();
    }
  };

  private mountUserReturn = async (user: User) => {
    return {
      id: user.id,
      name: user.name,
      nickname: user.nickname,
      isTeacher: user.isTeacher,
      active: user.active,
    };
  };

  private verifyUpdateStudent = async (userUpdate: IUserUpdate) => {
    const user = await await User.findOne({ where: { id: userUpdate.id } });
    if (!user) {
      throw new UserNotFoundException();
    }
    if (userUpdate.document && userUpdate.document != user.document) {
      const existDocument = await User.findOne({ where: { document: userUpdate.document } });
      if (existDocument) throw new UserAlreadyExistsException();
    }
    if (userUpdate.nickname && userUpdate.nickname != user.nickname) {
      const existNickname = await User.findOne({ where: { nickname: userUpdate.nickname } });
      if (existNickname) throw new UserAlreadyExistsException();
    }
  };
}

export default UserBusiness;
