export interface IUser {
  name: string;
  nickname: string;
  document: string;
  password: string;
  isTeacher?: boolean;
  active?: boolean;
}

export interface IUserUpdate {
  id: number;
  name?: string;
  nickname?: string;
  document?: string;
  isTeacher?: boolean;
  active?: boolean;
}

export interface IUserReturn {
  id?: number;
  name: string;
  nickname: string;
  isTeacher?: boolean;
  active?: boolean;
}
