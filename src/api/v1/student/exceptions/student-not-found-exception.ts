import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class StudentNotFoundException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Student not found',
      errorCode: 20023,
      statusCode: HTTPStatus.NOT_FOUND,
      userMessage: 'You attempted to get a Student, but did not find any',
    });
  }
}

export default StudentNotFoundException;
