import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class StudentAlreadyExistsException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Student already exists',
      errorCode: 20033,
      statusCode: HTTPStatus.CONFLICT,
      userMessage: 'You attempted to create a Student, but already exists',
    });
  }
}

export default StudentAlreadyExistsException;
