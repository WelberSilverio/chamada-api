import { decode } from 'base64-arraybuffer';

import { IStudent, IStudentUpdate } from './interfaces/student';
import Student from '../../../models/student';
import StudentNotFoundException from './exceptions/student-not-found-exception';
import StudentAlreadyExistsException from './exceptions/student-alread-exists-exception';
import FaceApiRequest from '../../../requests/face-api/face-api-request';

class StudentBusiness {
  public createStudent = async (student: IStudent) => {
    const image = decode(student.image);
    const imagePersisted = await FaceApiRequest.persistFace(image);
    FaceApiRequest.trainRecogniton();
    await this.verifyStudent(student.document);
    return await Student.create({
      name: student.name,
      email: student.email,
      document: student.document,
      active: student.active,
      faceId: imagePersisted,
    });
  };

  public updateStudent = async (student: IStudentUpdate) => {
    await this.verifyUpdateStudent(student);
    const { id, name, email, document, active } = student;
    await Student.update(
      {
        name,
        email,
        document,
        active,
      },
      {
        where: { id },
      },
    );
    return student;
  };

  public getStudentById = async (id: number) => {
    const student = ((await Student.findOne({ where: { id } })) as unknown) as Student;
    if (!student) {
      throw new StudentNotFoundException();
    }
    return student;
  };

  private verifyStudent = async (document: string) => {
    const student = ((await Student.findOne({ where: { document } })) as unknown) as Student;
    if (student) {
      throw new StudentAlreadyExistsException();
    }
  };

  private verifyUpdateStudent = async (studentUpdate: IStudentUpdate) => {
    const student = await this.getStudentById(studentUpdate.id);
    if (studentUpdate.document && studentUpdate.document != student.document) {
      await this.verifyStudent(studentUpdate.document);
    }
  };
}

export default StudentBusiness;
