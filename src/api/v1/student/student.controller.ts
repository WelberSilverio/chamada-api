import { Controller, Post, IHttp, Get, Put } from 'above-framework';
import HTTPStatus from 'http-status';

import StudentBusiness from './student.business';
import { StudentValidateSchema } from './student.schema';

@Controller('/students')
class StudentController {
  // @ts-ignore
  @Post('/', {
    description: 'Rota para incluir alunos',
    validate: StudentValidateSchema.createStudent,
    records: true,
  })
  public async createStudent({ reply, request: { payload } }: IHttp<typeof StudentValidateSchema.createStudent>) {
    const student = await new StudentBusiness().createStudent(payload);
    return reply.response(student).code(HTTPStatus.CREATED);
  }

  // @ts-ignore
  @Put('/', {
    description: 'Rota para atualizar alunos',
    validate: StudentValidateSchema.updateStudent,
    records: true,
  })
  public async updateStudent({ reply, request: { payload } }: IHttp<typeof StudentValidateSchema.updateStudent>) {
    const student = await new StudentBusiness().updateStudent(payload);
    return reply.response(student).code(HTTPStatus.OK);
  }

  // @ts-ignore
  @Get('/{id}', {
    description: 'Rota para encontrar aluno por Id',
    validate: StudentValidateSchema.getById,
    records: true,
  })
  public async getStudentById({
    reply,
    request: {
      params: { id },
    },
  }: IHttp<typeof StudentValidateSchema.getById>) {
    const student = await new StudentBusiness().getStudentById(id);
    return reply.response(student).code(HTTPStatus.OK);
  }
}

export default StudentController;
