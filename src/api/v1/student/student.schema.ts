import { Joi } from 'above-framework';

export const StudentValidateSchema = {
  createStudent: {
    payload: Joi.object()
      .keys({
        name: Joi.string().required(),
        email: Joi.string().required(),
        document: Joi.string()
          .required()
          .max(11),
        image: Joi.string().required(),
        active: Joi.boolean(),
      })
      .required(),
  },
  updateStudent: {
    payload: Joi.object()
      .keys({
        id: Joi.number().required(),
        name: Joi.string(),
        document: Joi.string(),
        email: Joi.string(),
        active: Joi.boolean(),
      })
      .required(),
  },
  getById: {
    params: Joi.object()
      .keys({
        id: Joi.number().required(),
      })
      .required(),
  },
  getByQuery: {
    query: Joi.object()
      .keys({
        id: Joi.number(),
        document: Joi.string(),
        name: Joi.string(),
      })
      .required(),
  },
};
