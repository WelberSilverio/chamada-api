export interface IStudent {
  name: string;
  email: string;
  document: string;
  image: string;
  active?: boolean;
}

export interface IStudentUpdate {
  id: number;
  name?: string;
  email?: string;
  document?: string;
  active?: boolean;
}
