import jwt from 'jsonwebtoken';

import Env from '../config/env';

const generateToken = (options?: string | Buffer | object) => {
  const result = jwt.sign(options || {}, Env.JWT, {
    noTimestamp: true,
  });
  return result;
};

export default generateToken;
