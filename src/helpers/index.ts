import boxen from 'boxen';
import chalk from 'chalk';

interface IRequired {
  required: boolean;
}

interface IDefault<T> {
  defaultValue: T;
}
type TOptions<T> = IRequired | IDefault<T>;

export function getEnv<T>(name: string, options?: TOptions<T>): T {
  const value: any = process.env[name];
  const info: any = options;
  // istanbul ignore next
  if (info && info.required && !value) {
    // istanbul ignore next
    // eslint-disable-next-line
    console.log(
      boxen(`A variavel de ambiente ${chalk.bold.redBright(name)} não foi encontrado!`, {
        padding: 1,
      }),
    );
  }
  // istanbul ignore next
  // eslint-disable-next-line
  if (value && !isNaN(value)) {
    // istanbul ignore next
    return (parseInt(String(value), 10) as unknown) as T;
  }
  // istanbul ignore next
  if (value === undefined && options && info.defaultValue) {
    // istanbul ignore next
    return info.defaultValue as T;
  }
  return value;
}
