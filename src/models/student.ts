import { Column, Model, PrimaryKey, Table, AutoIncrement, Unique } from 'sequelize-typescript';

export interface IStudent {
  id: number;
  name: string;
  email: string;
  document: string;
  active: boolean;
}

@Table({ tableName: 'student' })
class Student extends Model<Student> implements IStudent {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @Column
  public name: string;

  @Column
  public email: string;

  @Unique
  @Column
  public document: string;

  @Column
  public faceId: string;

  @Column
  public active: boolean;
}

export default Student;
