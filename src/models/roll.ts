import { Column, Model, PrimaryKey, Table, ForeignKey, BelongsTo, AutoIncrement } from 'sequelize-typescript';

import SchoolClassStudent from './schoolclass-student';

export interface IRoll {
  id: number;
  schoolclassStudentId: number;
  classDate: string;
  status: string;
}

@Table({ tableName: 'roll' })
class Roll extends Model<Roll> implements IRoll {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @ForeignKey(() => SchoolClassStudent)
  @Column
  public schoolclassStudentId: number;

  @Column
  public classDate: string;

  @Column
  public status: string;

  @BelongsTo(() => SchoolClassStudent)
  schoolClassStudent: SchoolClassStudent;
}

export default Roll;
