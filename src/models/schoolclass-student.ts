import { Column, Model, PrimaryKey, Table, ForeignKey, BelongsTo, AutoIncrement } from 'sequelize-typescript';

import SchoolClass from './schoolclass';
import Student from './student';

export interface ISchoolClassStudent {
  id: number;
  schoolclassId: number;
  studentId: number;
  status: string;
}

@Table({ tableName: 'schoolclass_student' })
class SchoolClassStudent extends Model<SchoolClassStudent> implements ISchoolClassStudent {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @ForeignKey(() => SchoolClass)
  @Column
  public schoolclassId: number;

  @ForeignKey(() => Student)
  @Column
  public studentId: number;

  @Column
  public status: string;

  @BelongsTo(() => SchoolClass)
  schoolclass: SchoolClass;

  @BelongsTo(() => Student)
  student: Student;
}

export default SchoolClassStudent;
