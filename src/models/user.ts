import { Column, Model, PrimaryKey, Table, Unique, AutoIncrement } from 'sequelize-typescript';

export interface IUser {
  id: number;
  name: string;
  document: string;
  nickname: string;
  password: string;
  isTeacher: boolean;
  active: boolean;
}

@Table({ tableName: 'user' })
class User extends Model<User> implements IUser {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @Column
  public name: string;

  @Unique
  @Column
  public document: string;

  @Unique
  @Column
  public nickname: string;

  @Column
  public password: string;

  @Column
  public isTeacher: boolean;

  @Column
  public active: boolean;
}

export default User;
