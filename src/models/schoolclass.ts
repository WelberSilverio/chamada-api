import { Column, Model, PrimaryKey, Table, ForeignKey, BelongsTo, AutoIncrement } from 'sequelize-typescript';

import User from './user';

export interface ISchoolClass {
  id: number;
  name: string;
  shift: string;
  period: string;
  day: string;
  teacherId: number;
  active: boolean;
}

@Table({ tableName: 'schoolclass' })
class SchoolClass extends Model<SchoolClass> implements ISchoolClass {
  @PrimaryKey
  @AutoIncrement
  @Column
  public id: number;

  @Column
  public name: string;

  @Column
  public shift: string;

  @Column
  public period: string;

  @Column
  public day: string;

  @ForeignKey(() => User)
  @Column
  public teacherId: number;

  @Column
  public active: boolean;

  @BelongsTo(() => User)
  user: User;
}

export default SchoolClass;
