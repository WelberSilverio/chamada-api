/* istanbul ignore file */
import { Bootstrap, Hapi } from 'above-framework';
import _ from 'lodash';

import generateToken from './helpers/generate-token';
import AplicationConfig from './config/application.config';

class BoostrapSingleton {
  private static server: Hapi.Server;

  private static routes: {
    key: any;
    server: Hapi.Server;
  }[] = [];

  private constructor() {}

  public static async getInstance(route?: any): Promise<Hapi.Server> {
    if (route) {
      let server: any = this.routes.find(item => _.isEqual(item.key, route));
      if (!server) {
        server = await Bootstrap({
          ...AplicationConfig,
          route,
        });
        this.routes = this.routes.concat([{ key: route, server }]);
      } else {
        return server.server;
      }
      return server;
    }

    if (!this.server) {
      this.server = await Bootstrap(AplicationConfig);
    }
    return this.server;
  }
}

interface IServerInjectOptions extends Hapi.ServerInjectOptions {
  jwtPayload?: {
    [x: string]: any;
  };
  controller?: any;
}

const ServerMock = async (options: IServerInjectOptions) => {
  const match = /^\/(v[0-9]+)/.exec(options.url);
  let route: any;
  if (options.controller && match) {
    route = {
      controller: options.controller,
      version: match[1],
    };
  }
  const server = await BoostrapSingleton.getInstance(route);
  // eslint-disable-next-line
  delete options.controller;

  const headers = options && options.headers ? options.headers : undefined;
  // eslint-disable-next-line
  delete options.headers;
  const optionsInject = {
    headers: {
      authorization: generateToken(options.jwtPayload),
      ...headers,
    },
    ...options,
  };
  const response = await server.inject(optionsInject);

  const payload: Record<string, any> = JSON.parse(response.payload);

  return {
    ...response,
    payload,
  };
};

export default ServerMock;
