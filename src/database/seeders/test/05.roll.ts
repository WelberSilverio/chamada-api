import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('roll', [
      {
        id: 1,
        schoolclass_student_id: 1,
        status: 'PRESENTE',
        class_date: '25/05/2020',
      },
      {
        id: 2,
        schoolclass_student_id: 3,
        status: 'AUSENTE',
        class_date: '25/05/2020',
      },
      {
        id: 3,
        schoolclass_student_id: 2,
        status: 'PRESENTE',
        class_date: '25/05/2020',
      },
      {
        id: 4,
        schoolclass_student_id: 4,
        status: 'AUSENTE',
        class_date: '25/05/2020',
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('roll', {}, {});
  },
};
