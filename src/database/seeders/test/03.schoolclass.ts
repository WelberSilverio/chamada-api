import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('schoolclass', [
      {
        id: 1,
        name: 'Engenharia de Software I',
        shift: 'manha',
        period: '1',
        day: 'segunda',
        teacher_id: 1,
        active: true,
      },
      {
        id: 2,
        name: 'Engenharia de Software II',
        shift: 'manha',
        period: '2',
        day: 'segunda',
        teacher_id: 1,
        active: true,
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('schoolclass', {}, {});
  },
};
