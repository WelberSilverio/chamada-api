import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('schoolclass_student', [
      {
        id: 1,
        schoolclass_id: 1,
        student_id: 1,
        status: 'CURSANDO',
      },
      {
        id: 2,
        schoolclass_id: 2,
        student_id: 1,
        status: 'CURSANDO',
      },
      {
        id: 3,
        schoolclass_id: 1,
        student_id: 2,
        status: 'CURSANDO',
      },
      {
        id: 4,
        schoolclass_id: 2,
        student_id: 2,
        status: 'CURSANDO',
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('schoolclass_student', {}, {});
  },
};
