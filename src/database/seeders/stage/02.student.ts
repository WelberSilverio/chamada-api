import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('student', [
      {
        id: 1,
        name: 'Aluninho ADS',
        document: '13213578456',
        email: 'aluninho@student.com',
        face_id: '8b121a4e-407a-4b91-9b90-4ba2592a6329',
        active: true,
      },
      {
        id: 2,
        name: 'Engenheiro ADS',
        document: '66666666854',
        email: 'engenheiro@student.com',
        face_id: '8b121a4e-407a-4b91-9b90-4ba2592a6323',
        active: true,
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('student', {}, {});
  },
};
