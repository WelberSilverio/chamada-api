import { QueryInterface } from 'sequelize';
import bcrypt from 'bcryptjs';

const salt = bcrypt.genSaltSync(10);

module.exports = {
  up: (queryInterface: QueryInterface) => {
    return queryInterface.bulkInsert('user', [
      {
        id: 1,
        name: 'Carlao',
        document: '13213578955',
        nickname: 'prof_carlos',
        password: bcrypt.hashSync('carloslucas9', salt),
        is_teacher: true,
        active: true,
      },
      {
        id: 2,
        name: 'Lord Varys',
        document: '66666666666',
        nickname: 'lord_varys',
        password: bcrypt.hashSync('secretaria13', salt),
        is_teacher: false,
        active: true,
      },
    ]);
  },

  down: (queryInterface: QueryInterface) => {
    return queryInterface.bulkDelete('user', {}, {});
  },
};
