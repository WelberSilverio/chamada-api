import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize) =>
    queryInterface.createTable('roll', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      schoolclassStudentId: {
        field: 'schoolclass_student_id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
      },
      classDate: {
        field: 'class_date',
        type: Sequelize.STRING(30),
        allowNull: false,
      },
      status: {
        field: 'status',
        type: Sequelize.STRING(30),
        allowNull: false,
        defaultValue: 'AUSENTE',
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('roll'),
};
