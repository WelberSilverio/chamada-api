import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize) =>
    queryInterface.createTable('schoolclass_student', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      schoolclassId: {
        field: 'schoolclass_id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
      },
      studentId: {
        field: 'student_id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
      },
      status: {
        field: 'status',
        type: Sequelize.STRING(30),
        allowNull: false,
        defaultValue: 'CURSANDO',
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('schoolclass_student'),
};
