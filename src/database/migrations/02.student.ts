import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize) =>
    queryInterface.createTable('student', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        field: 'name',
        type: Sequelize.STRING(200),
        allowNull: false,
      },
      document: {
        field: 'document',
        unique: true,
        type: Sequelize.STRING(11),
        allowNull: false,
      },
      email: {
        field: 'email',
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      faceId: {
        field: 'face_id',
        type: Sequelize.STRING(250),
        allowNull: false,
      },
      active: {
        field: 'active',
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('student'),
};
