import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize) =>
    queryInterface.createTable('user', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        field: 'name',
        type: Sequelize.STRING(200),
        allowNull: false,
      },
      document: {
        field: 'document',
        type: Sequelize.STRING(11),
        unique: true,
        allowNull: false,
      },
      nickname: {
        field: 'nickname',
        type: Sequelize.STRING(11),
        unique: true,
        allowNull: false,
      },
      password: {
        field: 'password',
        type: Sequelize.STRING(300),
        allowNull: false,
      },
      isTeacher: {
        field: 'is_teacher',
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: false,
      },
      active: {
        field: 'active',
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: true,
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('user'),
};
