import { QueryInterface } from 'sequelize';

module.exports = {
  up: (queryInterface: QueryInterface, Sequelize) =>
    queryInterface.createTable('schoolclass', {
      id: {
        field: 'id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        field: 'name',
        type: Sequelize.STRING(200),
        allowNull: false,
      },
      shift: {
        field: 'shift',
        type: Sequelize.STRING(11),
        allowNull: false,
      },
      day: {
        field: 'day',
        type: Sequelize.STRING(11),
        allowNull: false,
      },
      period: {
        field: 'period',
        type: Sequelize.STRING(2),
        allowNull: false,
      },
      teacherId: {
        field: 'teacher_id',
        type: Sequelize.INTEGER(11),
        allowNull: false,
      },
      active: {
        field: 'active',
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
    }),

  down: (queryInterface: QueryInterface) => queryInterface.dropTable('schoolclass'),
};
