import bunyan from 'bunyan';

const log = bunyan.createLogger({
  name: 'chamada',
  stream: process.stdout,
  level: 'debug',
});

const createLog = key => {
  return log.child({ key });
};

export default {
  createLog,
  log,
};
