import { Dialect } from 'sequelize/types';

import { getEnv } from '../helpers';

/* istanbul ignore file */
if (process.env.NODE_ENV === 'test') {
  require('dotenv').config({
    path: `${__dirname}/../../.env.testing`,
  });
} else {
  require('dotenv').config({
    path: `${__dirname}/../../.env`,
  });
}

const Env = {
  /* Configuração */
  JWT: getEnv<string>('JWT', { required: true }),
  NODE_ENV: getEnv<'test' | 'production' | 'stage' | 'development'>('NODE_ENV', {
    required: true,
  }),

  /* Banco de Dados */
  DB_USER: getEnv<string>('DB_USER', {
    required: true,
  }),
  DB_PASSWORD: getEnv<string>('DB_PASSWORD', {
    required: true,
  }),
  DB_DATABASE: getEnv<string>('DB_DATABASE', {
    required: true,
  }),
  DB_HOST: getEnv<string>('DB_HOST', {
    required: true,
  }),
  DB_CONNECTION: getEnv<Dialect>('DB_CONNECTION', {
    required: true,
  }),

  /* Requests */
  FACE_API_URI: getEnv<string>('FACE_API_URI', {
    required: true,
  }),
  FACE_API_KEY: getEnv<string>('FACE_API_KEY', {
    required: true,
  }),
  FACE_API_LIST: getEnv<string>('FACE_API_LIST', {
    required: true,
  }),
};

export default Env;
