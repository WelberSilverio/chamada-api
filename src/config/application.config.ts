import { IApplication } from 'above-framework';

import Sequelize from './sequelize';

const ApplicationConfig: IApplication = {
  database: Sequelize,
  plugins: [],
  path: `${__dirname}/../`,
};

export default ApplicationConfig;
