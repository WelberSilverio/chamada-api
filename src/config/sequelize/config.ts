import Env from '../env';

const Database = {
  username: Env.DB_USER,
  password: Env.DB_PASSWORD,
  database: Env.DB_DATABASE,
  host: Env.DB_HOST,
  dialect: Env.DB_CONNECTION,
};

module.exports = {
  [Env.NODE_ENV]: Database,
};
