import { Sequelize as SequelizeTS } from 'sequelize-typescript';

import Env from '../env';

const Sequelize = new SequelizeTS({
  database: Env.DB_DATABASE,
  dialect: Env.DB_CONNECTION,
  host: Env.DB_HOST,
  username: Env.DB_USER,
  password: Env.DB_PASSWORD,
  modelPaths: [`${__dirname}/../../models`],
  define: {
    timestamps: false,
    underscored: true,
  },
  logging: Env.NODE_ENV === 'development',
});

export default Sequelize;
