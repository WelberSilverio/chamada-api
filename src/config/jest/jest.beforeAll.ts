import nock from 'nock';

const setup = async () => {
  nock.disableNetConnect();
  nock.enableNetConnect(/^(127\.0\.0\.1|localhost|0\.0\.0\.0)/);
  nock.cleanAll();
};

export default setup;
