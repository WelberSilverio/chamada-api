import { create } from 'apisauce';

import Env from '../../config/env';

const FACE_API = create({
  baseURL: Env.FACE_API_URI,
  headers: {
    'Ocp-Apim-Subscription-Key': `${Env.FACE_API_KEY}`,
    'Content-Type': 'application/octet-stream',
  },
});

export default FACE_API;
