import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class FaceNotFoundException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Face not found',
      errorCode: 20023,
      statusCode: HTTPStatus.NOT_FOUND,
      userMessage: 'You attempted to get a Face, but did not find any',
    });
  }
}

export default FaceNotFoundException;
