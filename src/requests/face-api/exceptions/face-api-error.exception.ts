import { BaseException } from 'above-framework';
import HTTPStatus from 'http-status';

class FaceApiErrorException extends BaseException {
  constructor() {
    super({
      developerMessage: 'Internal server error',
      errorCode: 10000,
      statusCode: HTTPStatus.INTERNAL_SERVER_ERROR,
      userMessage: 'Was encountered an error when processing your request. We apologize for the inconvenience.',
    });
  }
}

export default FaceApiErrorException;
