import HTTPStatus from 'http-status';

import Env from '../../config/env';
import FACE_API from './face-api';
import API from './api';
import FaceNotFoundException from './exceptions/face-not-found.exception';
import FaceApiErrorException from './exceptions/face-api-error.exception';

interface IPostFaceApiRequest {
  persistedFaceId: string;
}

interface IPostFaceDetected {
  faceId: string;
}

class FaceApiRequest {
  public static async persistFace(image: ArrayBuffer) {
    const response = await FACE_API.post<IPostFaceApiRequest>(
      `/largefacelists/${Env.FACE_API_LIST}/persistedFaces`,
      image,
    );

    if (response.status === HTTPStatus.OK && response.data) {
      return response.data.persistedFaceId;
    }

    if (response.status === HTTPStatus.NOT_FOUND) {
      throw new FaceNotFoundException();
    }
    throw new FaceApiErrorException();
  }

  public static async trainRecogniton() {
    const response = await API.post<IPostFaceApiRequest>(`/largefacelists/${Env.FACE_API_LIST}/train`);
    if (response.status != HTTPStatus.ACCEPTED) {
      throw new FaceApiErrorException();
    }
  }

  public static async detectFace(image: ArrayBuffer) {
    const response = await FACE_API.post<IPostFaceDetected>(
      `/detect?returnFaceId=true&returnFaceLandmarks=false`,
      image,
    );
    if (response.status === HTTPStatus.OK && response.data) {
      return response.data[0].faceId;
    }

    if (response.status === HTTPStatus.NOT_FOUND) {
      throw new FaceNotFoundException();
    }
    throw new FaceApiErrorException();
  }

  public static async findSimilars(faceId: string) {
    const response = await API.post<IPostFaceApiRequest>(`/findsimilars`, {
      faceId,
      largeFaceListId: `${Env.FACE_API_LIST}`,
      maxNumOfCandidatesReturned: 2,
      mode: 'matchPerson',
    });
    if (response.status === HTTPStatus.OK && response.data) {
      return response.data[0].persistedFaceId;
    }

    if (response.status === HTTPStatus.NOT_FOUND) {
      throw new FaceNotFoundException();
    }
    throw new FaceApiErrorException();
  }
}

export default FaceApiRequest;
