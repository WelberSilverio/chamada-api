import { create } from 'apisauce';

import Env from '../../config/env';

const API = create({
  baseURL: Env.FACE_API_URI,
  headers: {
    'Ocp-Apim-Subscription-Key': `${Env.FACE_API_KEY}`,
    'Content-Type': 'application/json',
  },
});

export default API;
